<?php
namespace Neodimius\CustomContactUs\Model;

use Magento\Framework\Model\AbstractModel;

class CustomContactUs extends AbstractModel {
    protected function _construct() {
        $this->_init('Neodimius\CustomContactUs\Model\ResourceModel\CustomContactUs');
    }
}
