<?php
namespace Neodimius\CustomContactUs\Model\ResourceModel\CustomContactUs;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection {
    protected function _construct() {
        parent::_construct();
        $this->_init(
            'Neodimius\CustomContactUs\Model\CustomContactUs',
            'Neodimius\CustomContactUs\Model\ResourceModel\CustomContactUs'
        );
    }
}
