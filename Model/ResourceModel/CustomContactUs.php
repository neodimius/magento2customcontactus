<?php
namespace Neodimius\CustomContactUs\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class CustomContactUs extends AbstractDb {
    protected function _construct() {
        $this->_init('custom_contact_us', 'id');
    }
}
