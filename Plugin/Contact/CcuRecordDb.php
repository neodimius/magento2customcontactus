<?php
namespace Neodimius\CustomContactUs\Plugin\Contact;

use Magento\Contact\Model\Mail;

class CcuRecordDb
{
    /*
     * DB object
     */
    protected $collectionFactory;

    /**
     * CcuRecordDb constructor.
     * @param \Neodimius\CustomContactUs\Model\ResourceModel\CustomContactUs\CollectionFactory $collectionFactory
     */
    public function __construct(
        \Neodimius\CustomContactUs\Model\ResourceModel\CustomContactUs\CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @param Mail $subject
     * @param $replyTo
     * @param array $variables
     * @return array
     */
    public function beforeSend(Mail $subject, $replyTo, array $variables)
    {
        $this->collectionFactory->create();
        $this->collectionFactory->addData([
            "name" => $variables['data']['name'],
            "email" => $variables['data']['email'],
            "text" => $variables['data']['comment'],
        ]);
        $this->collectionFactory->save();

        return [$replyTo, $variables];
    }
}
